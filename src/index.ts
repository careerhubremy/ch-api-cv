import polka from "polka"
import shared from 'ch-api-ts-shared'
const server = polka();

server.use(shared.middleware.logger.LogToConsole)

server.get("/a", (req, res) => {
	res.end("Hi polka")
})
server.listen(3000, () => {
	console.log("Running on http://localhost:3000/")
})